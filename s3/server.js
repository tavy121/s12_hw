const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    },
    {
        id: 2,
        productName: 'Phone',
        price: 100
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/:id', (req,res,next)=>{
    const idPatchedProduct = req.params.id;
    let updated = false;
    products.forEach((product)=>{
        if(product.id == idPatchedProduct){
            updated = true;
            product.price = req.body.price;
            product.productName = req.body.productName;
        }
    });

    if(updated){
        res.status(200).send(`Produsul cu id-ul ${idPatchedProduct} a fost updatat`);
    }else{
        res.status(404).send(`Nu a fost gasit produsul cu id-ul ${idPatchedProduct}`);
    }
});

app.delete("/:productName", (req,res,next)=>{
    const deletedProductName = req.params.productName;
    let deleted = false;
    products.forEach((product) =>{
        if(product.productName == deletedProductName){
            products.splice(req.body.id , 1);
            deleted = true;
        }
    });
    if(deleted){
        res.status(200).send(`Produsul cu numele ${deletedProductName} a fost sters`);
    }else{
        res.status(404).send(`Nu a fost gasit produsul cu numele ${deletedProductName}`);
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});