import React, { Component } from 'react';
import './App.css';
import {ProductsList} from './components/ProductsList'
import {AddProduct} from './components/AddProduct'


class App extends Component {

  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [{productName: "ceva"}, {productName: "altceva"}];

  }

  onItemAdded = (product)=>{
    this.state.product.push(product);
    let products = this.state.products;
    this.setState({
      products: products
    });

    console.log(this.state(products));
  }

  render() {
    return (
      <React.Fragment>
        <h1>Product app</h1>
        <AddProduct />
        <div className="lists-container">
        <ProductsList title="Products" source={this.state.products} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
