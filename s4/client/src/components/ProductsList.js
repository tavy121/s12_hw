import React, {Component} from 'react';
import './ProductList.css';
import axios from 'axios';

export class ProductsList extends Component{
    constructor(props){
        super(props);
        this.state = {
            products: []
        }
    }

    componentWillMount(){
        this.getBibs();
    }

    getBibs(){
        axios.get('http://localhost:8090/get-all')
        .then(response =>{
            this.setState({products: response.data}, () =>{
                console.log(this.state);
            });
        })
        .catch(err => console.log(err));
    }

   render(){
       let items = this.state.products.map((item, index)=>
           <div key={index}>
            <h4>Phone name:{item.productName} </h4>
            Price {item.price}
           </div>
       );
       return (
            <React.Fragment>
                <div>
                    <h1>{this.props.title}</h1>
                    <div className="items-container">
                    {items}
                    </div>
                </div>
            </React.Fragment>
       )
   }
}
