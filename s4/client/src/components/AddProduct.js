import React, {Component} from 'react'
import axios from 'axios';

export class AddProduct extends Component{
    constructor(props){
        super(props);
        this.state ={
            productName: "",
            price: 0
        }
    }

    handleChangeProductName = (e)=>{
        this.setState({
            productName: e.target.value
        });
    }

    handleChangePrice = (e)=>{
        this.setState({
            price: e.target.value
        });
    }


    addProduct(newProduct){
        axios.request({
            method: 'post',
            url: 'http://localhost:8090/add',
            data: newProduct
        })
        .then(response =>{
            console.log(response);
        })
        .catch(err => console.log(err));
    }

    onSubmit(e){
        const newProduct = {
            productName: this.refs.prName.value,
            price: this.refs.price.value
        }
        this.addProduct(newProduct);
        e.preventDefault();
    }

    render(){
        return(
            <React.Fragment>
                <h1>Add product</h1>
                <div>
                <input type="text" placeholder="Product Name" name="prName" ref="prName" onChange={this.handleChangeProductName} />
                    <input type="number" placeholder="Price" name="price" ref="price" onChange={this.handleChangePrice}/>
                    <button onClick={this.onSubmit.bind(this)}>Add Product</button>
                </div>
            </React.Fragment>
        );
    }
}